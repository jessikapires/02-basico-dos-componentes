import React, { Component } from 'react'

class ProductPage extends Component {
    constructor(props){
        super(props)
        this.carrinho=this.carrinhodecompras.bind(this)
        this.carrinho1=this.carrinhodecompras1.bind(this)
        this.carrinho2=this.carrinhodecompras2.bind(this)
    } 
    carrinhodecompras(event){ /*botão para adicionar ao carrinho */
        event.preventDefault()
        alert("Produto no carrinho de compras: " + 
        this.props.product.skus[0].skuname + " por " + 
        this.props.product.skus[0].bestPriceFormated )
    }
    carrinhodecompras1(event){ /*botão para adicionar ao carrinho */
        event.preventDefault()
        alert("Produto no carrinho de compras: " + 
        this.props.product.skus[1].skuname + " por " + 
        this.props.product.skus[1].bestPriceFormated )
    }
    carrinhodecompras2(event){ /*botão para adicionar ao carrinho */
        event.preventDefault()
        alert("Produto no carrinho de compras: " + 
        this.props.product.skus[2].skuname + " por " + 
        this.props.product.skus[2].bestPriceFormated )
    }
    render () {
        return (
            <div>
                
                <h1>{this.props.product.skus[0].skuname}</h1>
                    <img src={this.props.product.skus[0].image}alt="img,png" width="500px"></img>
                        <h1><del>{this.props.product.skus[0].listPriceFormated}</del></h1>
                            <h1><mark>{this.props.product.skus[0].bestPriceFormated}</mark></h1>
                                <h1>estoque: {this.props.product.skus[0].availablequantity} peças</h1>
                                    <h1>{this.props.product.skus[0].seller}</h1>
                                    <h1>{this.props.product.skus[0].dimensionsMap}</h1>
                <button className="botao" onClick={this.carrinho}>comprar</button>   

                <h1>{this.props.product.skus[1].skuname}</h1>
                    <img src={this.props.product.skus[1].image}alt="img,png" width="500px"></img>
                        <h1><del>{this.props.product.skus[1].listPriceFormated}</del></h1>
                            <h1><mark>{this.props.product.skus[1].bestPriceFormated}</mark></h1>
                                <h1>estoque: {this.props.product.skus[1].availablequantity} peças</h1>
                                    <h1>{this.props.product.skus[1].seller}</h1>
                <button className="botao" onClick={this.carrinho1}>comprar</button>   

                <h1>{this.props.product.skus[2].skuname}</h1>
                    <img src={this.props.product.skus[2].image}alt="img,png" width="500px"></img>
                        <h1><del>{this.props.product.skus[2].listPriceFormated}</del></h1>
                            <h1><mark>{this.props.product.skus[2].bestPriceFormated}</mark></h1>
                                <h1>estoque: {this.props.product.skus[2].availablequantity} peças</h1>
                                    <h1>{this.props.product.skus[2].seller}</h1>
                <button className="botao" onClick={this.carrinho2}>comprar</button>   
                
                
            </div>
    

            
        )
    }
}

export default ProductPage