export default {
    "productId": 1167,
    "name": "Blusa BTS",
    "salesChannel": "1",
    "available": true,
    "dimensions": [
        "tamanho"
    
    ],
    "dimensionsMap": {
        "tamanho": [
            "P",
            "M",
            "G",
            "GG"
        ]
    },
    "skus": [
        {
            "sku": 4929,
            "skuname": " BLUSA BTS GATINHO PRETA",
            "dimensions": {
                "tamanho": " P"
            },
            "available": false, // se false, produto indisponivel, se true, disponivel
            "availablequantity": 10, //quantos estão no estoque
            "cacheVersionUsedToCallCheckout": null,
            "listPriceFormated": "De R$ 239,90", //preço inicial
            "listPrice": 239.90,
            "taxFormated": "R$ 0,00",
            "taxAsInt": 0,
            "bestPriceFormated": "Por R$ 199,90", //preço na promoção
            "bestPrice": 0,
            "installments": 0,
            "installmentsValue": 0,
            "installmentsInsterestRate": null,
            "image": "https://images-submarino.b2w.io/produtos/01/00/oferta/51207/9/51207948_1GG.jpg",
            "sellerId": "1",
            "seller": "Vestuario Corean",
            "measures": {
                "cubicweight": 1.0000,
                "height": 20.0000,
                "length": 10.0000,
                "weight": 800.0000,
                "width": 15.0000
            },
            "unitMultiplier": 1.0000,
            "rewardValue": 0

        },    /*fim da posição 1*/
        {
            "sku": 4930,
            "skuname": "BLUSA BTS GATINHO ROSA",
            "dimensions": {
                "tamanho": "M"
            },
            "available": false,
            "availablequantity": 8,
            "cacheVersionUsedToCallCheckout": null,
            "listPriceFormated": "De R$ 270,90",
            "listPrice": 239.90,
            "taxFormated": "R$ 0,00",
            "taxAsInt": 0,
            "bestPriceFormated": "Por R$ 180,00",
            "bestPrice": 0,
            "installments": 0,
            "installmentsValue": 0,
            "installmentsInsterestRate": null,
            "image": "https://http2.mlstatic.com/D_NP_674206-MLB29514961814_022019-Q.jpg",
            "sellerId": "1",
            "seller": "Vestuario Peripera",
            "measures": {
                "cubicweight": 1.0000,
                "height": 20.0000,
                "length": 10.0000,
                "weight": 800.0000,
                "width": 15.0000
            },
            "unitMultiplier": 1.0000,
            "rewardValue": 0

        },              /*fim da p 2*/
        {
            "sku": 4931,
            "skuname": "BLUSA BTS GATINHO PRETA COM ESTAMPA COLORIDA",
            "dimensions": {
                "tamanho": "G"
            },
            "available": true,
            "availablequantity": 3,
            "cacheVersionUsedToCallCheckout": "d227bc129516772a2d658db2217cfaf8_",
            "listPriceFormated": "De R$ 300,00",
            "listPrice": 239.90,
            "taxFormated": "R$ 0,00",
            "taxAsInt": 0,
            "bestPriceFormated": "Por R$ 270,90",
            "bestPrice": 23990,
            "installments": 12,
            "installmentsValue": 1999,
            "installmentsInsterestRate": 0,
            "image": "https://http2.mlstatic.com/D_NP_855062-MLB28186946499_092018-Q.jpg",
            "sellerId": "1",
            "seller": "Vestuario Sakura",
            "measures": {
                "cubicweight": 1.0000,
                "height": 20.0000,
                "length": 10.0000,
                "weight": 800.0000,
                "width": 15.0000
            },
            "unitMultiplier": 1.0000,
            "rewardValue": 0
        },          /*fim p3*/
        {
            "sku": 4932,
            "skuname": "Blusa BTS cor preta com detalhes coloridos",
            "dimensions": {
                "tamanho": "GG"
            },
            "available": true,
            "availablequantity": 5,
            "cacheVersionUsedToCallCheckout": "d227bc129516772a2d658db2217cfaf8_",
            "listPriceFormated": "R$ 200,90",
            "listPrice": 200.90,
            "taxFormated": "R$ 0,00",
            "taxAsInt": 0,
            "bestPriceFormated": "R$ 239,90",
            "bestPrice": 23990,
            "installments": 12,
            "installmentsValue": 1999,
            "installmentsInsterestRate": 0,
            "image": "http://vestaria.vteximg.com.br/arquivos/ids/162828-650-910/kit5-elite.jpg?v=636882691869600000",
            "sellerId": "1",
            "seller": "Vestaria Shop",
            "measures": {
                "cubicweight": 1.0000,
                "height": 20.0000,
                "length": 10.0000,
                "weight": 800.0000,
                "width": 15.0000
            },
            "unitMultiplier": 1.0000,
            "rewardValue": 0
        }
    ]
}